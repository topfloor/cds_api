<?php
/**
 * Created by PhpStorm.
 * User: BMcClure
 * Date: 1/10/2016
 * Time: 12:00 AM
 */

namespace TopFloor\Cds\SelectOptions;


interface CdsSelectOptionsInterface {
  public function getOptions();
}